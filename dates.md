---
order: 95
---
# Dates

There is a lot of dates in the MangaDex API this page aim to explain how all of them works and what they're used for

## CreatedAt

This date is present on all resources, it represents the date & time when the resource was created.

## UpdatedAt

This date is present on all resources, it represents the date & time when the resource was last updated.

## PublishAt

This date is present on chapter resources and represents the date & time when the content will be available on MangaDex.
If the chapter is an external link, this date represents when the chapter (and its pages) will be published on MangaDex, 
but before that the link will be visible on MangaDex.

## ReadableAt

This date is present on chapter resources and represents the date & time when the chapter is readable (even if it's a 
link out of MangaDex).
You could represent it as following:
- If publishAt is in future & chapter has externalUrl then readableAt is equals createdAt date
- Otherwise readableAt is equals publishAt date
