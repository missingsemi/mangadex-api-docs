---
label: Login
order: 100
---

# Login

!!!warning
**Deprecated!** MangaDex is moving past its regular authentication system, and will, in the future, utilize OAuth for secure user authentication. You can still log in using the old method, but it won't be possible to do so in the near future. For any questions regarding MangaDex authentication for third-party applications, join our [discord](https://discord.com/mangadex).
!!!

Mangadex allows you to personalize your feed, keep track of your reading activity, make lists of your favorite manga, and more. To login, you first need to have an account, which can only be made through the [website](https://mangadex.org/account/signup) as the endpoint is protected by ReCaptcha.

## Making a session

```http
 POST /auth/login
```

Logins are handled by serving a JWT session token along with an expiry date, and a refresh token with which we can use to make a new session without logging in again.

!!!info
The session token expires after 15 minutes, and the refresh token after 1 month.
!!!


##### Request

Supposing an account with username "testuser" and "testpass" exists, we proceed like this.

+++Python

:::code-block
```python
creds = {
    "username": "testuser",
    "password": "testpass",
}
```
:::

:::code-block
```python
import requests
from datetime import datetime

base_url = "https://api.mangadex.org"

r = requests.post(
    f"{base_url}/auth/login", 
    json=creds
)
r_json = r.json()

session_token = r_json["token"]["session"]
expires = datetime.now().timestamp() + 15 * 60000
refresh_token = r_json["token"]["refresh"]

print(session_token, expires, refresh_token)
```
:::

+++JavaScript

:::code-block
```javascript
const creds = {
    username: 'testuser',
    password: 'testpass'
};
```
:::

:::code-block
```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

let sessionToken, expires, refreshToken;

const resp = await axios({
    method: 'POST',
    url: `${baseUrl}/auth/login`,
    headers: {
        'Content-Type': 'application/json'
    },
    data: creds
});

sessionToken = resp.data.token.session;
expires = new Date().valueOf() + 15 * 60000
refreshToken = resp.data.token.refresh;

console.log(sessionToken, expires, refreshToken);

```
:::

+++

## Refreshing a session

```http
 POST /auth/refresh
```

Once a session expires, you have to call this endpoint to acquire a new one.


##### Request

+++Python

:::code-block
```python
refresh_token = "yourrefreshtokenhere"
```
:::

:::code-block
```python
import requests
from datetime import datetime

base_url = "https://api.mangadex.org"

r = requests.post(
    f"{base_url}/auth/refresh",
    json={"token": refresh_token},
)

session_token = r.json()["token"]["session"]
expires = datetime.now().timestamp() + 15 * 60000

print(session_token, expires, refresh_token)
```
:::

+++JavaScript

:::code-block
```javascript
const refreshToken = 'yourrefreshtokenhere';
```
:::

:::code-block
```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'POST',
    url: `${baseUrl}/auth/refresh`,
    headers: {
        'Content-Type': 'application/json'
    },
    data: {
        token: refreshToken
    }
});

const sessionToken = resp.data.token.session;
const expires = new Date().valueOf() + 15 * 60000

console.log(sessionToken, expires, refreshToken)

```
:::

+++
