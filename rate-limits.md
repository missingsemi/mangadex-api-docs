---
order: 99
---

# Rate limits

The API enforces rate limits to protect our servers against malicious and/or mistaken use. The API keeps track of the
requests on an IP-by-IP basis.
Hence, if you're on a VPN, proxy or a shared network in general, the requests of other users on this network might
affect you.

At first, a **global limit of 5 requests per second per IP address** is in effect.

> This limit is enforced across multiple load-balancers, and thus
> is not an exact value but rather a lower-bound that we guarantee. The exact value will be somewhere in the
> range `[5, 5*n]` (with `n` being the number of
> load-balancers currently active). The exact value within this range will depend on the current traffic patterns we are
> experiencing.

On top of this, **some endpoints are further restricted** as follows:

| Endpoint                        | Requests per time period | Time period in minutes { class="compact" } |
|---------------------------------|--------------------------|--------------------------------------------|
| **Account**                     |                          |                                            |
| `GET /account/available`        | 60                       | 60                                         |
| `POST /account/create`          | 5                        | 60                                         |
| `POST /account/activate/{code}` | 30                       | 60                                         |
| `POST /account/activate/resend` | 5                        | 60                                         |
| `POST /account/recover`         | 5                        | 60                                         |
| `POST /account/recover/{code}`  | 5                        | 60                                         |
| `POST /user/email`              | 1                        | 60                                         |
| `DELETE /user/{id}`             | 5                        | 60                                         |
| **AtHome** (MangaDex@Home)      |                          |                                            |
| `GET /at-home/server/{id}`      | 40                       | 1                                          |
| **Authentication**              |                          |                                            |
| `POST /auth/login`              | 30                       | 60                                         |
| `POST /auth/refresh`            | 60                       | 60                                         |
| **Author**                      |                          |                                            |
| `POST /author`                  | 10                       | 60                                         |
| `PUT /author`                   | 10                       | 1                                          |
| `DELETE /author/{id}`           | 10                       | 10                                         |
| **Captcha** (reCaptcha)         |                          |                                            |
| `POST /captcha/solve`           | 10                       | 10                                         |
| **Cover**                       |                          |                                            |
| `POST /cover`                   | 100                      | 10                                         |
| `PUT /cover/{id}`               | 100                      | 10                                         |
| `DELETE /cover/{id}`            | 10                       | 10                                         |
| **Chapter**                     |                          |                                            |
| `POST /chapter/{id}/read`       | 300                      | 10                                         |
| `PUT /chapter/{id}`             | 10                       | 1                                          |
| `DELETE /chapter/{id}`          | 10                       | 1                                          |
| **Forums**                      |                          |                                            |
| `POST /forums/thread`           | 10                       | 1                                          |
| **Manga**                       |                          |                                            |
| `POST /manga`                   | 10                       | 60                                         |
| `PUT /manga/{id}`               | 10                       | 60                                         |
| `DELETE /manga/{id}`            | 10                       | 10                                         |
| `POST /manga/draft/{id}/commit` | 10                       | 60                                         |
| `GET /manga/random`             | 60                       | 1                                          |
| **Reports**                     |                          |                                            |
| `POST /report`                  | 10                       | 1                                          |
| `GET /report`                   | 10                       | 1                                          |
| **ScanlationGroup**             |                          |                                            |
| `POST /group`                   | 10                       | 60                                         |
| `PUT /group/{id}`               | 10                       | 1                                          |
| `DELETE /group/{id}`            | 10                       | 10                                         |
| **Upload**                      |                          |                                            |
| -> Sessions                     |                          |                                            |
| `GET /upload`                   | 30                       | 1                                          |
| `POST /upload/begin`            | 20  (shared)             | 1                                          |
| `POST /upload/begin/{id}`       | 20  (shared)             | 1                                          |
| `POST /upload/{id}/commit`      | 10                       | 1                                          |
| `DELETE /upload/{id}`           | 30                       | 1                                          |
| -> Files                        |                          |                                            |
| `POST /upload/{id}`             | 250 (shared)             | 1                                          |
| `DELETE /upload/{id}/{id}`      | 250 (shared)             | 1                                          |
| `DELETE /upload/{id}/batch`     | 250 (shared)             | 1                                          |

Calling these endpoints will further provide details via the following headers about your remaining quotas:

| Header                    | Description                                                                |
|---------------------------|----------------------------------------------------------------------------|
| `X-RateLimit-Limit`       | Maximal number of requests this endpoint allows per its time period        |
| `X-RateLimit-Remaining`   | Remaining number of requests within your quota for the current time period |
| `X-RateLimit-Retry-After` | Timestamp of the end of the current time period, as UNIX timestamp         |

# Result Limit

Most of our listing endpoints will return a maximum number of total results that is currently capped at 10.000 items.
Beyond that you will not receive any more items no matter how far you paginate and the results will become empty
instead. This is for performance reasons and a limitation we will not lift.

Note that the limit is applied to a search query and list endpoints with or without any filters are search queries.
If you need to retrieve more items, use filters to narrow down your search.

# Further limitations

From time to time, we may apply undocumented limits to some endpoints if we deem the traffic patterns abusive. This can
range from cache-busting attempts to persistent incorrect requests causing API error spikes.

Similarly, some specific request patterns associated with malicious intent result in instantly having your IP blocked
for a period of time.

Finally, retrying requests upon being rate-limited will result in a combination of the rate limit being progressively
extended until your requests are dropped entirely at the network layer.
